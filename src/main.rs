use termion::event::Key;
use termion::event::Event::Key as KeyEvent;
use std::io::stdin;
use termion::{
    *,
    raw::IntoRawMode,
    input::TermRead,
};
use std::io::{Write, stdout};

mod ui;

fn main() {
    let stdout = stdout();
    let mut stdout = stdout.lock().into_raw_mode().unwrap();
    write!(stdout, "{}{}", clear::All, cursor::Goto(1,1)).unwrap();
    stdout.flush().unwrap();

    //Let's init our menu
    let mut menubar = ui::menu::MenuBar::new();
    menubar.add_menu(&mut stdout, ui::menu::Menu::new(String::from("File"), String::from("File")));
    menubar.draw_bar(&mut stdout);
    stdout.flush().unwrap();
    for e in stdin().events() {
        match e.unwrap() {
            KeyEvent(k) => {
                match k {
                    Key::Esc | Key::Char('m') => {
                        menubar.activate_menu();
                    }
                    Key::Char(c) => {

                    },
                    _ => {}
                }
            },
            e => {
                write!(stdout, "{}{:?}", cursor::Goto(1,5), e).unwrap();
            }
        }
        stdout.flush().unwrap();
    }

}
