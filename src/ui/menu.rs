//! A menu system for our browser. This uses a 'tick' system to keep the menu updated.


use std::io::Write;
use termion::{
    *,
    raw::IntoRawMode,
    input::TermRead,
    color::{Bg, Fg}
};

pub struct Menu {
    name: String,
    text: String,
    menu_items: Vec<MenuItem>,
    selected_item: usize,
}

pub struct MenuBar {
    menus: Vec<Menu>,
    selected_item: Option<(usize, bool)>
}

pub struct MenuItem {
    name: String,
    text: String,
    key_binding: usize,
}

impl MenuBar {
    pub fn new() -> MenuBar {
        MenuBar { menus: Vec::new(), selected_item: None }
    }
    pub fn add_menu(&mut self, stdout: &mut Write, menu: Menu) {
        self.menus.push(menu);
    }

    pub fn draw_bar(&self, stdout: &mut Write) {
        let (term_x, _) = termion::terminal_size().unwrap();
        write!(stdout, "{}{}{}", cursor::Goto(1,1), Bg(color::White), Fg(color::Black)).unwrap();
        //Write a full white background over the top of the terminal
        for _ in 0..term_x {
            write!(stdout, " ").unwrap();
        }
        write!(stdout, "{}", cursor::Goto(1,1)).unwrap();
        //And now we write all the menu_items
        for menu in &self.menus {
            write!(stdout, " {} ", menu.text).unwrap();
        }
    }

    pub fn activate_menu(&mut self) {
        self.selected_item = Some((0, false));
    }

    pub fn deactivate_menu(&mut self) {
        self.selected_item = None
    }

    pub fn toggle_menu(&mut self) {
        match self.selected_item {
            Some(_) => self.deactivate_menu(),
            None => self.activate_menu()
        }
    }
}

impl Menu {
    pub fn new(name: String, text: String) -> Menu {
        Menu {name, text, menu_items: Vec::new(), selected_item: 0}
    }
}
