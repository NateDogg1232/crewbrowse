# crewbrowse

Crewbrowse is a terminal based web browser written entirely in Rust. It's meant for use in systems where there is either no graphical display or for people who just like to live in the terminal, as I do.

## Compiling
To compile, make sure you have the Rust compiler and Cargo installed. The recommended way of doing this is through [rustup](https://rustup.rs/). Then make sure the beta toolchain is installed by running the command `rustup toolchain add beta`

Once this is finished, run the command: `cargo +beta build`.

## Contributing

To contribute, check the issue list for a list of things to do and add. If it's empty, try adding a new feature. New features, as long as they don't regress others, are always welcome.
